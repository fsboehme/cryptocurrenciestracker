/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

'use strict';


// create
var RgridApplication = angular.module('CryptoCurrenciesTracker', ['officeuifabric.core', 'officeuifabric.components']);

console.log('Init dropdowns');
var DropdownHTMLElements = document.querySelectorAll('.ms-Dropdown');
for (var i = 0; i < DropdownHTMLElements.length; ++i) {
  var Dropdown = new fabric['Dropdown'](DropdownHTMLElements[i]);
}

//This is the controller I write. The other is generated code that i will need to understand/kill
RgridApplication.controller('CryptoController', function ($scope, $http) {
  console.log('Entering RgridApplication.controller CryptoController');

  //////////////////////////////////////////////////////////////////////////////////
  //This is a copy past from Google add IN so I try to keep code common. Do not edit
  //////////////////////////////////////////////////////////////////////////////////
  var aCryptoXchanges = [];
  //My exchange
  var aGemini = {};
  aGemini.name = "gemini";
  aGemini.url = "https://api.gemini.com/v1/pubticker/";
  aGemini.pair = { "BTCUSD": "btcusd", "ETHUSD": "ethusd" };
  aGemini.constructUrl = function (iCryptoPair) {
    return aGemini.url + aGemini.pair[iCryptoPair]
  };
  aGemini.parseResponse = function (iJsonResponse, iCryptoPair) {
    return iJsonResponse.last
  };
  aCryptoXchanges.push(aGemini);

  var aKraken = {};
  aKraken.name = "kraken";
  aKraken.url = "https://api.kraken.com/0/public/Ticker?pair=";
  aKraken.pair = { "BTCUSD": "XXBTZUSD", "ETHUSD": "XETHZUSD", "XRPUSD": "XXRPZUSD" };
  aKraken.constructUrl = function (iCryptoPair) {
    return aKraken.url + aKraken.pair[iCryptoPair]
  };
  aKraken.parseResponse = function (iJsonResponse, iCryptoPair) {
    return iJsonResponse.result[aKraken.pair[iCryptoPair]].c[0]
  };
  aCryptoXchanges.push(aKraken);

  var aGdax = {};
  aGdax.name = "gdax";
  aGdax.url = "https://api.gdax.com/products/";
  aGdax.pair = { "BTCUSD": "BTC-USD", "ETHUSD": "ETH-USD" };
  aGdax.constructUrl = function (iCryptoPair) {
    return aGdax.url + aGdax.pair[iCryptoPair] + "/ticker"
  };
  aGdax.parseResponse = function (iJsonResponse, iCryptoPair) {
    return iJsonResponse.price
  };
  aCryptoXchanges.push(aGdax);

  var aBitfinex = {};
  aBitfinex.name = "bitfinex";
  aBitfinex.url = "https://api.bitfinex.com/v1/pubticker/";
  aBitfinex.pair = { "BTCUSD": "btcusd", "ETHUSD": "ethusd", "XRPUSD": "xrpusd" };
  aBitfinex.constructUrl = function (iCryptoPair) {
    return aBitfinex.url + aBitfinex.pair[iCryptoPair]
  };
  aBitfinex.parseResponse = function (iJsonResponse, iCryptoPair) {
    return iJsonResponse.last_price
  };
  aCryptoXchanges.push(aBitfinex);

  var aPoloniex = {};
  aPoloniex.name = "poloniex";
  aPoloniex.url = "https://poloniex.com/public?command=returnTicker";
  aPoloniex.pair = { "BTCUSD": "USDT_BTC", "ETHUSD": "USDT_ETH", "XRPUSD": "USDT_XRP" };
  aPoloniex.constructUrl = function (iCryptoPair) {
    return aPoloniex.url
  };
  aPoloniex.parseResponse = function (iJsonResponse, iCryptoPair) {
    return iJsonResponse[aPoloniex.pair[iCryptoPair]].last
  };
  aCryptoXchanges.push(aPoloniex);

  /**
 * Verify if a key pair exists on a provider.
 *
 * @param {string} iKey crypto pair to test (BTCUSD, ETHUSD).
 * @param {string} iProviderName provider to use (kraken or gemini).
 * @return true if the keypair exists, False otherwise.
 */
  function isKeyValidForProvider(iKey, iProviderName) {
    var aProvider = aCryptoXchanges.filter(function (aOneProvider) {
      return aOneProvider.name == iProviderName;
    });
    if (aProvider.length == 1) {
      //Logger.log("Pairs are: " + aProvider[0].pair);
      if (iKey in aProvider[0].pair) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      //Logger.log("Provider " + iProviderName + " not found");
      return false;
    }
  }

  /**
   * get a provider.
   *
   * @param {string} iProviderName provider to use (kraken or gemini).
   * @return the provider.
   */
  function getProvider(iProviderName) {
    var aProvider = aCryptoXchanges.filter(function (aOneProvider) {
      return aOneProvider.name == iProviderName;
    });
    if (aProvider.length == 1) {
      return aProvider[0];
    }
    else {
      //Logger.log("Provider " + iProviderName + " not found");
      return false;
    }
  }
  ////////////////////////////////////////////////////////////////////////
  //End of the copy past to have common code between Google and MS plugin
  ////////////////////////////////////////////////////////////////////////

  /**
 * Update one binding.
 *
 * @param {object} iBinding Binding to update.
 */
  $scope.updateBinding = function (iBinding) {
    console.log("Update the binding: ", iBinding);

    Office.context.document.bindings.getByIdAsync(iBinding._uuid,
      function (asyncResult) {
        if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
          // TODO: Handle error
          console.log("Can not get the bindings - Deal with it");
        }
        else {

          var aBinding = asyncResult.value;
          console.log("I have the binding: ", aBinding);

          var aProviderObj = getProvider(iBinding._selectedExchange);
          var aUrl = aProviderObj.constructUrl(iBinding._selectedPair);

          console.log("Getting the value for exchange: " + iBinding._selectedExchange + " and pair: " + iBinding._selectedPair);
          if (isKeyValidForProvider(iBinding._selectedPair, iBinding._selectedExchange) == false) {
            throw new Error("Error - unknow iCryptoPair: " + iBinding._selectedPair + " for the provider: " + iBinding._selectedExchange);
          }

          $http.get(aUrl)
            .then(function (response) {
              var data = response.data;
              console.log("Data received: ", data);
              //var aResponseJson = JSON.parse(aResponseString);
              var aValue = aProviderObj.parseResponse(data, iBinding._selectedPair);
              console.log("aValue: ", aValue);
              aBinding.setDataAsync(aValue, function (asyncResult) { });
            });
        }
      });
  };

  /**
  * Update the bindings.
  *
  */
  $scope.updateBindings = function () {
    console.log("Update the bindings");
    var aBindingsFromDoc = Office.context.document.settings.get($scope._bindingsKeyInContext);
    console.log("Bindings from context: ", aBindingsFromDoc);
    aBindingsFromDoc.forEach(function (aOneBinding) {
      $scope.updateBinding(aOneBinding);
    });
  };

  /**
  * Clear the bindings.
  *
  */
  $scope.clearBindings = function () {
    console.log("Clear the bindings");
    var aBindingsFromDoc = Office.context.document.settings.get($scope._bindingsKeyInContext);
    console.log("Bindings from context: ", aBindingsFromDoc);
    aBindingsFromDoc.forEach(function (aOneBinding) {
      $scope.clearBinding(aOneBinding);
    });
    aBindingsFromDoc = [];
    Office.context.document.settings.set($scope._bindingsKeyInContext, aBindingsFromDoc);
  };

  /**
  * Clear one binding.
  *
  * @param {object} iBinding Binding to clear.
  */
  $scope.clearBinding = function (iOneBinding) {
    console.log("Clear the binding: ", iOneBinding);
    Office.context.document.bindings.releaseByIdAsync(iOneBinding._uuid, function (asyncResult) {
      //persist state
      persistSettings();
      console.log("Released: ", iOneBinding._uuid);
    });
  };


  /**
   * Click handler to trig bindings creation.
   *
   */
  $scope.getValue = function () {
    //Just create the bindings here. Nothing more
    createBinding();
  };

  /**
   * Genereate an UUID.
   *
   * @return UUID.
   */
  $scope.uuidv4 = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  /**
   * Create bindings with selected cell.
   *
   */
  function createBinding() {
    var uuid = $scope.uuidv4();
    var aNewBinding = { '_uuid': uuid, '_selectedExchange': $scope._selectedExchange, '_selectedPair': $scope._selectedPair };
    console.log('Creating a new binding: ', aNewBinding);
    Office.context.document.bindings.addFromSelectionAsync(
      Office.BindingType.Text, { id: uuid }, function (asyncResult) {
        if (asyncResult.status !== Office.AsyncResultStatus.Succeeded) {
          // TODO: Handle error
          console.log('Can not create the bindings');
        }
        else {
          // We only need to create the binding once, so let's disable
          // this functionality at this point
          console.log('Binding OK');
          var aCurrentBindings = Office.context.document.settings.get($scope._bindingsKeyInContext);
          if (aCurrentBindings == null) {
            // There are no bindings in the document. Creating a empty container
            aCurrentBindings = [];
          }
          aCurrentBindings.push(aNewBinding);
          console.log('aCurrentBindings', aCurrentBindings);
          //Writting it back in the document context
          Office.context.document.settings.set($scope._bindingsKeyInContext, aCurrentBindings);
          //persist state
          persistSettings();
        }
      });
  }

  function persistSettings() {
    console.log('Entering persistSettings');
    Office.context.document.settings.saveAsync(function (asyncResult) {
      console.log('Settings saved with status: ' + asyncResult.status);
    });
  }

  //The list of crypto exchange and pair. Comes from the copy past of Google plugin
  $scope._cryptoXchanges = aCryptoXchanges;
  //Extract the info from the _cryptoXchanges
  $scope._exchanges = $scope._cryptoXchanges.map(function (aOneExchange) { return aOneExchange.name });
  //This info should also be retrieve from _cryptoXchanges
  $scope._currenciesPairs = ["BTCUSD", "ETHUSD", "XRPUSD"];
  $scope._selectedExchange = "0";
  $scope._selectedPair = "0";
  $scope._version = "10";
  //Allow to save info in the document
  $scope._bindingsKeyInContext = "gg";
});


// when Office has initalized, manually bootstrap the app
Office.initialize = function () {
  console.log('Entering Office.initialize');
  angular.bootstrap(document.body, ['crypto-currencies-tracker']);
};

