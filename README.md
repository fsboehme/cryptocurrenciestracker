# CryptoCurrenciesTracker #

This Google/Microsoft spreadsheet add-on will create new function in google spreadsheet/MS office to retrieve value of cryptocurrencies from various providers. 

Supported providers: gemini, kraken, gdax, bitfinex, and poloniex  
Supported cryptocurrencies pairs: BTCUSD, ETHUSD, XRPUSD  
Some combinations are not possible (for example: Ripple is not available on Gemini provider). In this case the function will return an error with a detail explanation.  

### Usage ###

See the readme files in the respective addon folders.

+ [Google spreadsheet addon](./GooglePlugin/)
+ [MS office addon](./MicrosoftPlugin/)

### Contribution guidelines ###

Please ensure to run the tests before submitting a change request.
Any PR is welcome! 