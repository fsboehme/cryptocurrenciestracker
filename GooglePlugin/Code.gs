var aCryptoXchanges = [];
//My exchange
var aGemini={};
aGemini.name="gemini";
aGemini.url="https://api.gemini.com/v1/pubticker/";
aGemini.pair={"BTCUSD":"btcusd", "ETHUSD":"ethusd"};
aGemini.constructUrl=function(iCryptoPair){
return aGemini.url + aGemini.pair[iCryptoPair]};
aGemini.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.last};
aCryptoXchanges.push(aGemini);

var aKraken={};
aKraken.name="kraken";
aKraken.url="https://api.kraken.com/0/public/Ticker?pair=";
aKraken.pair={"BTCUSD":"XXBTZUSD", "ETHUSD":"XETHZUSD", "XRPUSD":"XXRPZUSD"};
aKraken.constructUrl=function(iCryptoPair){
return aKraken.url + aKraken.pair[iCryptoPair]};
aKraken.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.result[aKraken.pair[iCryptoPair]].c[0]};
aCryptoXchanges.push(aKraken);

var aGdax={};
aGdax.name="gdax";
aGdax.url="https://api.gdax.com/products/";
aGdax.pair={"BTCUSD":"BTC-USD", "ETHUSD":"ETH-USD", "LTCUSD":"LTC-USD"};
aGdax.constructUrl=function(iCryptoPair){
return aGdax.url + aGdax.pair[iCryptoPair] + "/ticker"};
aGdax.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.price};
aCryptoXchanges.push(aGdax);

var aBitfinex={};
aBitfinex.name="bitfinex";
aBitfinex.url="https://api.bitfinex.com/v1/pubticker/";
aBitfinex.pair={"BTCUSD":"btcusd", "ETHUSD":"ethusd", "XRPUSD":"xrpusd"};
aBitfinex.constructUrl=function(iCryptoPair){
return aBitfinex.url + aBitfinex.pair[iCryptoPair]};
aBitfinex.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.last_price};
aCryptoXchanges.push(aBitfinex);

var aPoloniex={};
aPoloniex.name="poloniex";
aPoloniex.url="https://poloniex.com/public?command=returnTicker";
aPoloniex.pair={"BTCUSD":"USDT_BTC", "ETHUSD":"USDT_ETH", "XRPUSD":"USDT_XRP"};
aPoloniex.constructUrl=function(iCryptoPair){
return aPoloniex.url};
aPoloniex.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse[aPoloniex.pair[iCryptoPair]].last};
aCryptoXchanges.push(aPoloniex);

var aHitbtc={};
aHitbtc.name="hitbtc";
aHitbtc.url="https://api.hitbtc.com/api/2/public/ticker/";
aHitbtc.pair = {
  "BCNBTC":"BCNBTC",
  "BTCUSD":"BTCUSD",
  "DASHBTC":"DASHBTC",
  "DOGEBTC":"DOGEBTC",
  "DOGEUSD":"DOGEUSD",
  "DSHBTC":"DSHBTC",
  "EMCBTC":"EMCBTC",
  "ETHBTC":"ETHBTC",
  "FCNBTC":"FCNBTC",
  "LSKBTC":"LSKBTC",
  "LTCBTC":"LTCBTC",
  "LTCUSD":"LTCUSD",
  "NXTBTC":"NXTBTC",
  "QCNBTC":"QCNBTC",
  "SBDBTC":"SBDBTC",
  "SCBTC":"SCBTC",
  "STEEMBTC":"STEEMBTC",
  "XDNBTC":"XDNBTC",
  "XEMBTC":"XEMBTC",
  "XMRBTC":"XMRBTC",
  "ARDRBTC":"ARDRBTC",
  "ZECBTC":"ZECBTC",
  "WAVESBTC":"WAVESBTC",
  "MAIDBTC":"MAIDBTC",
  "AMPBTC":"AMPBTC",
  "BUSBTC":"BUSBTC",
  "DGDBTC":"DGDBTC",
  "ICNBTC":"ICNBTC",
  "SNGLSBTC":"SNGLSBTC",
  "1STBTC":"1STBTC",
  "XLCBTC":"XLCBTC",
  "TRSTBTC":"TRSTBTC",
  "TIMEBTC":"TIMEBTC",
  "GNOBTC":"GNOBTC",
  "REPBTC":"REPBTC",
  "XMRUSD":"XMRUSD",
  "DASHUSD":"DASHUSD",
  "ETHUSD":"ETHUSD",
  "NXTUSD":"NXTUSD",
  "ZRCBTC":"ZRCBTC",
  "BOSBTC":"BOSBTC",
  "DCTBTC":"DCTBTC",
  "ANTBTC":"ANTBTC",
  "AEONBTC":"AEONBTC",
  "GUPBTC":"GUPBTC",
  "PLUBTC":"PLUBTC",
  "LUNBTC":"LUNBTC",
  "TAASBTC":"TAASBTC",
  "NXCBTC":"NXCBTC",
  "EDGBTC":"EDGBTC",
  "RLCBTC":"RLCBTC",
  "SWTBTC":"SWTBTC",
  "TKNBTC":"TKNBTC",
  "WINGSBTC":"WINGSBTC",
  "XAURBTC":"XAURBTC",
  "AEBTC":"AEBTC",
  "PTOYBTC":"PTOYBTC",
  "WTTBTC":"WTTBTC",
  "ZECUSD":"ZECUSD",
  "XEMUSD":"XEMUSD",
  "BCNUSD":"BCNUSD",
  "XDNUSD":"XDNUSD",
  "MAIDUSD":"MAIDUSD",
  "ETCBTC":"ETCBTC",
  "ETCUSD":"ETCUSD",
  "CFIBTC":"CFIBTC",
  "PLBTBTC":"PLBTBTC",
  "BNTBTC":"BNTBTC",
  "XDNCOBTC":"XDNCOBTC",
  "FYNETH":"FYNETH",
  "SNMETH":"SNMETH",
  "SNTETH":"SNTETH",
  "CVCUSD":"CVCUSD",
  "PAYETH":"PAYETH",
  "OAXETH":"OAXETH",
  "OMGETH":"OMGETH",
  "BQXETH":"BQXETH",
  "XTZBTC":"XTZBTC",
  "CRSUSD":"CRSUSD",
  "DICEBTC":"DICEBTC",
  "CFIETH":"CFIETH",
  "PTOYETH":"PTOYETH",
  "1STETH":"1STETH",
  "XAURETH":"XAURETH",
  "TAASETH":"TAASETH",
  "TIMEETH":"TIMEETH",
  "DICEETH":"DICEETH",
  "SWTETH":"SWTETH",
  "XMRETH":"XMRETH",
  "ETCETH":"ETCETH",
  "DASHETH":"DASHETH",
  "ZECETH":"ZECETH",
  "PLUETH":"PLUETH",
  "GNOETH":"GNOETH",
  "XRPBTC":"XRPBTC",
  "NETETH":"NETETH",
  "STRATUSD":"STRATUSD",
  "STRATBTC":"STRATBTC",
  "SNCETH":"SNCETH",
  "ADXETH":"ADXETH",
  "BETETH":"BETETH",
  "EOSETH":"EOSETH",
  "DENTETH":"DENTETH",
  "SANETH":"SANETH",
  "EOSBTC":"EOSBTC",
  "EOSUSD":"EOSUSD",
  "MNEBTC":"MNEBTC",
  "MRVETH":"MRVETH",
  "MSPETH":"MSPETH",
  "DDFETH":"DDFETH",
  "XTZETH":"XTZETH",
  "XTZUSD":"XTZUSD",
  "UETETH":"UETETH",
  "MYBETH":"MYBETH",
  "SURETH":"SURETH",
  "IXTETH":"IXTETH",
  "HRBETH":"HRBETH",
  "PLRETH":"PLRETH",
  "TIXETH":"TIXETH",
  "NDCETH":"NDCETH",
  "PROETH":"PROETH",
  "AVTETH":"AVTETH",
  "COSSETH":"COSSETH",
  "PBKXETH":"PBKXETH",
  "PQTUSD":"PQTUSD",
  "8BTUSD":"8BTUSD",
  "EVXUSD":"EVXUSD",
  "IMLETH":"IMLETH",
  "ROOTSETH":"ROOTSETH",
  "DLTBTC":"DLTBTC",
  "BNTETH":"BNTETH",
  "BNTUSD":"BNTUSD",
  "QAUBTC":"QAUBTC",
  "QAUETH":"QAUETH",
  "MANAUSD":"MANAUSD",
  "DNTBTC":"DNTBTC",
  "FYPBTC":"FYPBTC",
  "OPTBTC":"OPTBTC",
  "GRPHBTC":"GRPHBTC",
  "TNTETH":"TNTETH",
  "STXBTC":"STXBTC",
  "STXETH":"STXETH",
  "STXUSD":"STXUSD",
  "TNTUSD":"TNTUSD",
  "TNTBTC":"TNTBTC",
  "CATBTC":"CATBTC",
  "CATETH":"CATETH",
  "CATUSD":"CATUSD",
  "BCHBTC":"BCHBTC",
  "BCHETH":"BCHETH",
  "BCHUSD":"BCHUSD",
  "ECATETH":"ECATETH",
  "XUCUSD":"XUCUSD",
  "SNCBTC":"SNCBTC",
  "SNCUSD":"SNCUSD",
  "OAXUSD":"OAXUSD",
  "OAXBTC":"OAXBTC",
  "BASETH":"BASETH",
  "ZRXBTC":"ZRXBTC",
  "ZRXETH":"ZRXETH",
  "ZRXUSD":"ZRXUSD",
  "RVTBTC":"RVTBTC",
  "ICOSBTC":"ICOSBTC",
  "ICOSETH":"ICOSETH",
  "ICOSUSD":"ICOSUSD",
  "PPCBTC":"PPCBTC",
  "PPCUSD":"PPCUSD",
  "QTUMETH":"QTUMETH",
  "VERIBTC":"VERIBTC",
  "VERIETH":"VERIETH",
  "VERIUSD":"VERIUSD",
  "IGNISETH":"IGNISETH",
  "PRGBTC":"PRGBTC",
  "PRGETH":"PRGETH",
  "PRGUSD":"PRGUSD",
  "BMCBTC":"BMCBTC",
  "BMCETH":"BMCETH",
  "BMCUSD":"BMCUSD",
  "CNDBTC":"CNDBTC",
  "CNDETH":"CNDETH",
  "CNDUSD":"CNDUSD",
  "SKINBTC":"SKINBTC",
  "EMGOBTC":"EMGOBTC",
  "EMGOUSD":"EMGOUSD",
  "CDTETH":"CDTETH",
  "CDTUSD":"CDTUSD",
  "FUNBTC":"FUNBTC",
  "FUNETH":"FUNETH",
  "FUNUSD":"FUNUSD",
  "HVNBTC":"HVNBTC",
  "HVNETH":"HVNETH",
  "FUELBTC":"FUELBTC",
  "FUELETH":"FUELETH",
  "FUELUSD":"FUELUSD",
  "POEBTC":"POEBTC",
  "POEETH":"POEETH",
  "MCAPBTC":"MCAPBTC",
  "AIRBTC":"AIRBTC",
  "AIRETH":"AIRETH",
  "AIRUSD":"AIRUSD",
  "AMBUSD":"AMBUSD",
  "AMBETH":"AMBETH",
  "AMBBTC":"AMBBTC",
  "NTOBTC":"NTOBTC",
  "ICOBTC":"ICOBTC",
  "PINGBTC":"PINGBTC",
  "RKCETH":"RKCETH",
  "GAMEBTC":"GAMEBTC",
  "TKRETH":"TKRETH",
  "HPCBTC":"HPCBTC",
  "PPTETH":"PPTETH",
  "MTHBTC":"MTHBTC",
  "MTHETH":"MTHETH",
  "WMGOBTC":"WMGOBTC",
  "WMGOUSD":"WMGOUSD",
  "LRCBTC":"LRCBTC",
  "LRCETH":"LRCETH",
  "ICXBTC":"ICXBTC",
  "ICXETH":"ICXETH",
  "NEOBTC":"NEOBTC",
  "NEOETH":"NEOETH",
  "NEOUSD":"NEOUSD",
  "CSNOBTC":"CSNOBTC",
  "ORMEBTC":"ORMEBTC",
  "ICXUSD":"ICXUSD",
  "PIXBTC":"PIXBTC",
  "PIXETH":"PIXETH",
  "INDETH":"INDETH",
  "KICKBTC":"KICKBTC",
  "YOYOWBTC":"YOYOWBTC",
  "MIPSBTC":"MIPSBTC",
  "CDTBTC":"CDTBTC",
  "XVGBTC":"XVGBTC",
  "XVGETH":"XVGETH",
  "XVGUSD":"XVGUSD",
  "DGBBTC":"DGBBTC",
  "DGBETH":"DGBETH",
  "DGBUSD":"DGBUSD",
  "DCNETH":"DCNETH",
  "DCNUSD":"DCNUSD",
  "LATBTC":"LATBTC",
  "CCTETH":"CCTETH",
  "EBETETH":"EBETETH",
  "VIBEBTC":"VIBEBTC",
  "VOISEBTC":"VOISEBTC",
  "ENJBTC":"ENJBTC",
  "ENJETH":"ENJETH",
  "ENJUSD":"ENJUSD",
  "ZSCBTC":"ZSCBTC",
  "ZSCETH":"ZSCETH",
  "ZSCUSD":"ZSCUSD",
  "ETBSBTC":"ETBSBTC",
  "TRXBTC":"TRXBTC",
  "TRXETH":"TRXETH",
  "TRXUSD":"TRXUSD",
  "VENBTC":"VENBTC",
  "VENETH":"VENETH",
  "VENUSD":"VENUSD",
  "ARTBTC":"ARTBTC",
  "EVXBTC":"EVXBTC",
  "EVXETH":"EVXETH",
  "QVTETH":"QVTETH",
  "EBTCOLDBTC":"EBTCOLDBTC",
  "EBTCOLDETH":"EBTCOLDETH",
  "EBTCOLDUSD":"EBTCOLDUSD",
  "BKBBTC":"BKBBTC",
  "EXNBTC":"EXNBTC",
  "TGTBTC":"TGTBTC",
  "ATSETH":"ATSETH",
  "UGTBTC":"UGTBTC",
  "UGTETH":"UGTETH",
  "UGTUSD":"UGTUSD",
  "CTRBTC":"CTRBTC",
  "CTRETH":"CTRETH",
  "CTRUSD":"CTRUSD",
  "BMTBTC":"BMTBTC",
  "BMTETH":"BMTETH",
  "SUBBTC":"SUBBTC",
  "SUBETH":"SUBETH",
  "SUBUSD":"SUBUSD",
  "WTCBTC":"WTCBTC",
  "CNXBTC":"CNXBTC",
  "ATBBTC":"ATBBTC",
  "ATBETH":"ATBETH",
  "ATBUSD":"ATBUSD",
  "ODNBTC":"ODNBTC",
  "BTMBTC":"BTMBTC",
  "BTMETH":"BTMETH",
  "BTMUSD":"BTMUSD",
  "B2XBTC":"B2XBTC",
  "B2XETH":"B2XETH",
  "B2XUSD":"B2XUSD",
  "ATMBTC":"ATMBTC",
  "ATMETH":"ATMETH",
  "ATMUSD":"ATMUSD",
  "LIFEBTC":"LIFEBTC",
  "VIBBTC":"VIBBTC",
  "VIBETH":"VIBETH",
  "VIBUSD":"VIBUSD",
  "DRTETH":"DRTETH",
  "STUUSD":"STUUSD",
  "HDGETH":"HDGETH",
  "OMGBTC":"OMGBTC",
  "PAYBTC":"PAYBTC",
  "COSSBTC":"COSSBTC",
  "PPTBTC":"PPTBTC",
  "SNTBTC":"SNTBTC",
  "BTGBTC":"BTGBTC",
  "BTGETH":"BTGETH",
  "BTGUSD":"BTGUSD",
  "SMARTBTC":"SMARTBTC",
  "SMARTETH":"SMARTETH",
  "SMARTUSD":"SMARTUSD",
  "XUCETH":"XUCETH",
  "XUCBTC":"XUCBTC",
  "CLBTC":"CLBTC",
  "CLETH":"CLETH",
  "CLUSD":"CLUSD",
  "LAETH":"LAETH",
  "CLDBTC":"CLDBTC",
  "CLDETH":"CLDETH",
  "CLDUSD":"CLDUSD",
  "ELMBTC":"ELMBTC",
  "EDOBTC":"EDOBTC",
  "EDOETH":"EDOETH",
  "EDOUSD":"EDOUSD",
  "HGTETH":"HGTETH",
  "POLLBTC":"POLLBTC",
  "IXTBTC":"IXTBTC",
  "PREBTC":"PREBTC",
  "ATSBTC":"ATSBTC",
  "SCLBTC":"SCLBTC",
  "BCCBTC":"BCCBTC",
  "BCCETH":"BCCETH",
  "BCCUSD":"BCCUSD",
  "ATLBTC":"ATLBTC",
  "EBTCNEWBTC":"EBTCNEWBTC",
  "EBTCNEWETH":"EBTCNEWETH",
  "EBTCNEWUSD":"EBTCNEWUSD",
  "ETPBTC":"ETPBTC",
  "ETPETH":"ETPETH",
  "ETPUSD":"ETPUSD",
  "OTXBTC":"OTXBTC",
  "CDXETH":"CDXETH",
  "DRPUBTC":"DRPUBTC",
  "NEBLBTC":"NEBLBTC",
  "NEBLETH":"NEBLETH",
  "HACBTC":"HACBTC",
  "CTXBTC":"CTXBTC",
  "CTXETH":"CTXETH",
  "ELEBTC":"ELEBTC",
  "ARNBTC":"ARNBTC",
  "ARNETH":"ARNETH",
  "SISABTC":"SISABTC",
  "SISAETH":"SISAETH",
  "STUBTC":"STUBTC",
  "STUETH":"STUETH",
  "GVTETH":"GVTETH",
  "INDIBTC":"INDIBTC",
  "BTXBTC":"BTXBTC",
  "BTXUSDT":"BTXUSDT",
  "LTCETH":"LTCETH",
  "BCNETH":"BCNETH",
  "MAIDETH":"MAIDETH",
  "NXTETH":"NXTETH",
  "STRATETH":"STRATETH",
  "XDNETH":"XDNETH",
  "XEMETH":"XEMETH",
  "PLRBTC":"PLRBTC",
  "SURBTC":"SURBTC",
  "BQXBTC":"BQXBTC",
  "DOGEETH":"DOGEETH",
  "ITSBTC":"ITSBTC",
  "AMMBTC":"AMMBTC",
  "AMMETH":"AMMETH",
  "AMMUSD":"AMMUSD",
  "DBIXBTC":"DBIXBTC",
  "PRSBTC":"PRSBTC",
  "KBRBTC":"KBRBTC",
  "TBTBTC":"TBTBTC",
  "EROBTC":"EROBTC",
  "SMSBTC":"SMSBTC",
  "SMSETH":"SMSETH",
  "SMSUSD":"SMSUSD",
  "ZAPBTC":"ZAPBTC",
  "DOVBTC":"DOVBTC",
  "DOVETH":"DOVETH",
  "FRDBTC":"FRDBTC",
  "OTNBTC":"OTNBTC",
  "CAPPBTC":"CAPPBTC",
  "CAPPETH":"CAPPETH",
  "XRPETH":"XRPETH",
  "XRPUSDT":"XRPUSDT",
  "CAPPUSDT":"CAPPUSDT",
  "HSRBTC":"HSRBTC",
  "LENDBTC":"LENDBTC",
  "LENDETH":"LENDETH",
  "SPFETH":"SPFETH",
  "LOCBTC":"LOCBTC",
  "LOCETH":"LOCETH",
  "LOCUSD":"LOCUSD",
  "BTCABTC":"BTCABTC",
  "BTCAETH":"BTCAETH",
  "BTCAUSD":"BTCAUSD",
  "WRCBTC":"WRCBTC",
  "WRCETH":"WRCETH",
  "WRCUSD":"WRCUSD",
  "SWFTCBTC":"SWFTCBTC",
  "SWFTCETH":"SWFTCETH",
  "SWFTCUSD":"SWFTCUSD",
  "SBTCBTC":"SBTCBTC",
  "SBTCETH":"SBTCETH",
  "STORMBTC":"STORMBTC",
  "STARETH":"STARETH",
  "SBTCUSDT":"SBTCUSDT" 
}; 
//https://api.hitbtc.com/api/2/public/symbol for all symbols
aHitbtc.constructUrl=function(iCryptoPair){
return aHitbtc.url + aHitbtc.pair[iCryptoPair]};
aHitbtc.parseResponse=function(iJsonResponse, iCryptoPair){
return iJsonResponse.last};
aCryptoXchanges.push(aHitbtc);


var aVersion="8";

/**
 * A special function that runs when the spreadsheet is open, used to add a
 * custom menu to the spreadsheet.
 */
function onOpen() {}

/**
 * Retrieve value of cryptocurrency.
 *
 * @param {string} iProvider provider to use (kraken, gemini, gdax, bitfinex, or poloniex).
 * @param {string} iCryptoPair crypto pair to retrieve (BTCUSD, ETHUSD, or XRPUSD).
 * @return The cryptocurrency value in desired fiat.
 * @example GET_CRYPTO("gemini"; "BTCUSD")
 * @customfunction
 */
function GET_CRYPTO(iProvider, iCryptoPair) {
  // Generic entry point for GET.
  //Logger.log("Entering GET_CRYPTO for provider: ", iProvider, " and pair: ", iCryptoPair);
  var aValue="0";
  //Before starting the real process we check if the iCryptoPair exists for the iProvider
  if(isKeyValidForProvider(iCryptoPair, iProvider) == false){
    throw new Error( "Error - unknown iCryptoPair: " + iCryptoPair + " for the provider: "+ iProvider);
  }
  aProviderObj=getProvider(iProvider);
  var aUrl = aProviderObj.constructUrl(iCryptoPair);
  var aResponse = UrlFetchApp.fetch(aUrl);
  var aResponseString = aResponse.getContentText();
  var aResponseJson = JSON.parse(aResponseString);
  //Logger.log(aResponseJson);
  //Parse the json response with the provider parser
  var aValue = aProviderObj.parseResponse(aResponseJson, iCryptoPair);
  //We need to convert as a int
  var aValueAsFloat = parseFloat(aValue)
  return aValueAsFloat;
}

/**
 * Retrieve the version of the script.
 *
 * @return the version of the script.
 * @example GET_CRYPTO()
 * @customfunction
 */
function GET_VERSION() {
  return aVersion;
}